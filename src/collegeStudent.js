import Student from "./student";

class CollegeStudent extends Student {
    constructor(name, dateOfBirth, homeTown, school, className,phoneNumber, specialized, studentCode) {
        super(name, dateOfBirth, homeTown, phoneNumber, school, className);
        this.specialized = specialized;
        this.studentCode = studentCode;
        
    }
}

export default CollegeStudent;