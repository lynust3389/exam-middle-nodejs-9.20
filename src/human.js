class Human {
    constructor(name, dateOfBirth, homeTown) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.homeTown = homeTown;
    }
};

export default Human;