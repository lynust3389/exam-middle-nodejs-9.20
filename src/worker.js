import Human from "./human";

class Worker extends Human {
    constructor(name, dateOfBirth, homeTown, job, salary, workPlace) {
        super(name, dateOfBirth, homeTown);
        this.job = job;
        this.salary = salary;
        this.workPlace = workPlace;
    }
}

export default Worker;