import Human from "./human";

class Student extends Human {
    constructor(name, dateOfBirth, homeTown, school, className, phoneNumber) {
        super(name, dateOfBirth, homeTown);
        this.school = school;
        this.className = className;
        this.phoneNumber = phoneNumber;
    }
}

export default Student;