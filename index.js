import Human from "./src/human";
import Student from "./src/student";
import CollegeStudent from "./src/collegeStudent";
import Worker from "./src/worker";

console.log(Human);
console.log(Student);
console.log(CollegeStudent);
console.log(Worker);

console.log(Student instanceof Human);
console.log(CollegeStudent instanceof Human);
console.log(CollegeStudent instanceof Student);
console.log(Worker instanceof Human);
console.log(Worker instanceof Student);
console.log(Worker instanceof CollegeStudent);

